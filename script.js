const btn = document.querySelector("button");
btn.addEventListener("click", randomRgbColor);

function randomInteger(max) {
    return Math.floor(Math.random() * (max + 1));
}

function randomRgbColor() {
    let r = randomInteger(255);
    let g = randomInteger(255);
    let b = randomInteger(255);
    let color = `rgb(${r},${g},${b})`
    document.querySelector("body").style.backgroundColor = color;
    document.querySelector("h1").innerText = color;
    btn.style.backgroundColor =color;
    // console.log(color);
}


